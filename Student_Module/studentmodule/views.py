from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.http import HttpResponseRedirect

# Create your views here.

def index(request):
    return render(request,'pages/index.html')
def login(request):
    return render(request,'pages/login.html')
def courses(request):
    return render(request,'pages/courses.html')
def join(request):
    return render(request,'pages/join.html')
def register(request):
    return render(request,'pages/register.html')
def mypage(request):
    return render(request,'pages/mypage.html')
