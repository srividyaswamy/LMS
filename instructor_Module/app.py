from flask import Flask, render_template, url_for, request, session, redirect
from flask.ext.pymongo import PyMongo
import bcrypt
from flask import jsonify

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'mongologinexample'
app.config['MONGO_URI'] = 'mongodb://Aakanksha:lms123@ds111618.mlab.com:11618/mysite'

'''app.config['MONGO_DBNAME'] = 'instructors'
app.config['MONGO_URI'] = 'mongodb://instructor:instructor@ds117178.mlab.com:17178/instructors'
'''
mongo = PyMongo(app)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/loginvalidate', methods=['POST'])
def loginvalidate():
    instructor = mongo.db.instructor
    login_user = instructor.find_one({'name' : request.form['username']})

    if login_user:
        hashpass = bcrypt.hashpw(request.form['pass'].encode('utf-8'), bcrypt.gensalt())
        if bcrypt.hashpw(bytes(request.form['pass'], 'utf-8'), hashpass) == hashpass:
            session['username'] = request.form['username']
            return render_template('course.html')
    return 'Invalid username/password combination'

@app.route('/addcourse',methods=['GET','POST'])
def addcourse():
    if request.method == 'POST':
        courses  = mongo.db.courses
        existing_course = courses.find_one({'courseName' : request.form['coursename']})

        if existing_course is None:
          courses.insert({'courseName' : request.form['coursename'], 'required' : request.form['Knowledge'],'Duration' : request.form['duration'],'Price' :request.form['price']})
          return 'ADDED!!'
        return 'That course already exists!'
    return render_template('dashboard.html') 
    

@app.route('/register', methods=['GET'])
def register():
    if request.method == 'POST':
        instructor  = mongo.db.instructor
        existing_user = instructor.find_one({'name' : request.form['username']})

        if existing_user is None:
          hashpass = bcrypt.hashpw(request.form['pass'].encode('utf-8'), bcrypt.gensalt())
          instructor.insert({'name' : request.form['username'], 'password' : hashpass})
          session['username'] = request.form['username']
          return redirect(url_for('index'))
        return 'That username already exists!'
    return render_template('register.html') 

@app.route('/myprofile')
def myprofile():
    instructor  = mongo.db.instructor
    existing_user = instructor.find_one({'name' : request.form['username']})
    return render_template('myprofile.html',existing_user=existing_user)


@app.route('/courses')
def courses():
    return render_template('courses.html')

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/mypage')
def mypage():
    return render_template('mypage.html')

@app.route('/dashboard')
def dashboard():
    return render_template('dashboard.html')

@app.route('/settings')
def settings():
    return render_template('dashboard.html')


    

@app.route('/logout')
def logout():
   # remove the username from the session if it is there
   session.pop('username', None)
   return redirect(url_for('index'))
if __name__ == '__main__':
    app.secret_key = 'mysecret'
app.run(debug=True)
